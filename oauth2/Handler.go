package oauth2

import (
	"bitbucket/fsgarciab/find-me-api/models"

	"github.com/jinzhu/gorm"
	"gopkg.in/oauth2.v3/errors"
)

func NewHandler(db *gorm.DB) Handler {
	return Handler{
		db: db,
	}
}

type Handler struct {
	db *gorm.DB
}

func (h *Handler) PasswordAuthorizationHandler(username string, password string) (clientID string, err error) {
	var userOrm models.User
	result := h.db.Where("tag = ?", username).First(&userOrm)
	if result.RecordNotFound() {
		return "", errors.ErrAccessDenied
	}
	userRequest := models.User{
		Tag:      username,
		Password: password,
	}
	if userOrm.VerifyPassword(&userRequest) {
		return userOrm.Tag, nil
	}
	return "", errors.ErrAccessDenied
}
