package models

import "time"

type Location struct {
	ID        int `gorm:"primary_key"`
	Latitude  float64
	Longitude float64
	User      User `gorm:"foreignkey:users_id"`
	CreatedAt time.Time
}
