package models

import (
	"errors"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID        int    `gorm:"primary_key"`
	Name      string `gorm:"size:255"`
	Tag       string
	Password  string
	Locations []Location
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}

func (u *User) GeneratePassword() (bool, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return false, errors.New("Could generate Password")
	}
	u.Password = string(hash)
	return true, nil
}

func (u *User) VerifyPassword(user *User) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(user.Password))
	return err == nil
}
