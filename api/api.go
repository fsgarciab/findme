package api

import (
	"bitbucket/fsgarciab/find-me-api/config"
	"bitbucket/fsgarciab/find-me-api/location"
	locationRepo "bitbucket/fsgarciab/find-me-api/location/repository"
	"bitbucket/fsgarciab/find-me-api/user"
	userRepo "bitbucket/fsgarciab/find-me-api/user/repository"

	foauth "bitbucket/fsgarciab/find-me-api/oauth2"

	"github.com/gin-gonic/gin"
	"github.com/go-oauth2/gin-server"
	"gopkg.in/oauth2.v3/manage"
	"gopkg.in/oauth2.v3/models"
	aserver "gopkg.in/oauth2.v3/server"
	"gopkg.in/oauth2.v3/store"
)

type api struct {
	router *gin.Engine
	repo   Dependecies
}

type Response struct {
	Data    interface{}
	Message string
}

type Dependecies struct {
	UserRepo     user.UserRepository
	LocationRepo location.LocationRepository
}

func NewApiInstance() *api {
	router := gin.Default()

	return &api{
		router: router,
	}
}

func (a *api) InstallApi() {

	manager := manage.NewDefaultManager()
	// token store
	manager.MustTokenStorage(store.NewMemoryTokenStore())

	clientStore := store.NewClientStore()
	clientStore.Set("000000", &models.Client{
		ID:     "000000",
		Secret: "999999",
		Domain: "http://localhost",
	})
	manager.MapClientStorage(clientStore)
	db, _ := config.Connect()
	//Handler
	handlerOAuth := foauth.NewHandler(db)

	// Initialize the oauth2 service
	server.InitServer(manager)
	server.SetAllowGetAccessRequest(true)
	server.SetClientInfoHandler(aserver.ClientFormHandler)
	server.SetPasswordAuthorizationHandler(handlerOAuth.PasswordAuthorizationHandler)

	auth := a.router.Group("/oauth2")
	{
		auth.GET("/token", server.HandleTokenRequest)
	}

	api := a.router.Group("/api")
	{
		api.Use(server.HandleTokenVerify())
		api.Use(a.LoadUserInContext()) //TODO: incomplete function
		api.GET("/users", a.FetchUser)
		api.GET("/users/:userID/byTag", a.GetUserByTag)
		api.GET("/users/:userID", a.GetUserByID)

		api.GET("/locations/:locationID", a.GetLocationByID)
		api.GET("/locations", a.FetchLocation)

	}

	a.router.POST("/users", a.SignIn)

}

func (a *api) WireDependencies() {
	db, err := config.Connect()
	if err != nil {
		panic(err)
	}
	a.repo.UserRepo = userRepo.NewUserRepository(db)
	a.repo.LocationRepo = locationRepo.NewLocationRepository(db)
}

func (a *api) RunApi() {
	a.router.Run()
}

func (a *api) Response(model interface{}, message string) *Response {
	return &Response{
		Data:    model,
		Message: message,
	}
}
