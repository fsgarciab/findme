package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (a *api) FetchLocation(ctx *gin.Context) {
	locations, _ := a.repo.LocationRepo.Fetch(ctx)
	ctx.JSON(200, a.Response(locations, "OK"))
}

func (a *api) GetLocationByID(ctx *gin.Context) {
	location, err := a.repo.LocationRepo.GetByID(ctx)
	if err != nil {
		ctx.JSON(http.StatusNotFound, a.Response(err, "Not Found"))
		return
	}
	ctx.JSON(http.StatusOK, a.Response(location, "OK"))
}
