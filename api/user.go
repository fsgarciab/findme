package api

import (
	"bitbucket/fsgarciab/find-me-api/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (a *api) FetchUser(ctx *gin.Context) {
	users, _ := a.repo.UserRepo.Fetch(ctx)
	ctx.JSON(200, a.Response(users, "OK"))
}

func (a *api) GetUserByID(ctx *gin.Context) {
	user, err := a.repo.UserRepo.GetByID(ctx)
	if err != nil {
		ctx.JSON(http.StatusNotFound, a.Response(err, "Not Found"))
		return
	}
	ctx.JSON(http.StatusOK, a.Response(user, "OK"))
}

func (a *api) GetUserByTag(ctx *gin.Context) {
	user, err := a.repo.UserRepo.GetByTag(ctx)
	if err != nil {
		ctx.JSON(http.StatusNotFound, a.Response(err, "Not Found"))
		return
	}
	ctx.JSON(http.StatusOK, a.Response(user, "OK"))
}

func (a *api) SignIn(ctx *gin.Context) {
	var userRequest models.User
	if err := ctx.ShouldBindJSON(&userRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, a.Response(err, "Not a Valid Request"))
		return
	}

	userRequest.GeneratePassword()
	a.repo.UserRepo.Store(ctx, &userRequest)
	ctx.JSON(http.StatusOK, a.Response(nil, "OK"))
}

func (a *api) LoadUserInContext() gin.HandlerFunc {
	return func(c *gin.Context) {
		accesstoken, _ := c.Get("AccessToken")

		fmt.Printf("%+v\n", accesstoken)
		fmt.Printf("%+v\n", c.Params)
		// user, err := a.repo.UserRepo.GetByID(12)
		// c.Set("User", user)
		// c.Next()
	}
}
