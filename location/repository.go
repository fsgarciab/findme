package location

import (
	"bitbucket/fsgarciab/find-me-api/models"

	"github.com/gin-gonic/gin"
)

type LocationRepository interface {
	Fetch(ctx *gin.Context) (*[]models.Location, error)
	GetByID(ctx *gin.Context) (*models.Location, error)
	Store(ctx *gin.Context, a *models.Location) (*models.Location, error)
}
