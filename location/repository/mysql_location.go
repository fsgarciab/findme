package repository

import (
	"bitbucket/fsgarciab/find-me-api/location"
	"bitbucket/fsgarciab/find-me-api/models"
	"errors"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type locationRepository struct {
	db *gorm.DB
}

func NewLocationRepository(db *gorm.DB) location.LocationRepository {
	return &locationRepository{
		db: db,
	}
}

func (u *locationRepository) Fetch(ctx *gin.Context) (*[]models.Location, error) {
	locationsORM := &[]models.Location{}
	u.db.Find(&locationsORM)
	fmt.Printf("%+v\n", locationsORM)
	return locationsORM, nil
}

func (u *locationRepository) GetByID(ctx *gin.Context) (*models.Location, error) {
	var locationORM models.Location
	result := u.db.Where("id = ?", ctx.Params.ByName("locationID")).First(&locationORM)
	if result.RecordNotFound() {
		return nil, errors.New("Not Found")
	}

	if result.Error != nil {
		if result.RecordNotFound() {
			return nil, result.Error
		}
		return nil, errors.New("Unknown error")
	}

	return &locationORM, nil
}

func (u *locationRepository) Store(ctx *gin.Context, location *models.Location) (*models.Location, error) {
	u.db.Create(&location)
	return location, nil
}
