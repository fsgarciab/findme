package main

import "bitbucket/fsgarciab/find-me-api/api"

func main() {

	api := api.NewApiInstance()
	api.InstallApi()
	api.WireDependencies()
	api.RunApi()

}
