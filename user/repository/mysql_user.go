package repository

import (
	"bitbucket/fsgarciab/find-me-api/models"
	"bitbucket/fsgarciab/find-me-api/user"
	"errors"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type userRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) user.UserRepository {
	return &userRepository{
		db: db,
	}
}

func (u *userRepository) Fetch(ctx *gin.Context) (*[]models.User, error) {
	usersORM := &[]models.User{}
	u.db.Find(&usersORM)
	fmt.Printf("%+v\n", usersORM)
	return usersORM, nil
}

func (u *userRepository) GetByID(ctx *gin.Context) (*models.User, error) {
	var userORM models.User
	result := u.db.Where("id = ?", ctx.Params.ByName("userID")).First(&userORM)
	if result.RecordNotFound() {
		return nil, errors.New("Not Found")
	}

	if result.Error != nil {
		if result.RecordNotFound() {
			return nil, result.Error
		}
		return nil, errors.New("Unknown error")
	}

	return &userORM, nil
}

func (u *userRepository) GetByTag(ctx *gin.Context) (*models.User, error) {
	var userORM models.User
	result := u.db.Where("tag = ?", ctx.Params.ByName("userID")).First(&userORM)
	if result.RecordNotFound() {
		return nil, errors.New("Not Found")
	}

	if result.Error != nil {
		if result.RecordNotFound() {
			return nil, result.Error
		}
		return nil, errors.New("Unknown error")
	}

	return &userORM, nil
}

func (u *userRepository) Store(ctx *gin.Context, user *models.User) (*models.User, error) {
	u.db.Create(&user)
	return user, nil
}
