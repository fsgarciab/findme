package user

import (
	"bitbucket/fsgarciab/find-me-api/models"

	"github.com/gin-gonic/gin"
)

type UserRepository interface {
	Fetch(ctx *gin.Context) (*[]models.User, error)
	GetByID(ctx *gin.Context) (*models.User, error)
	GetByTag(ctx *gin.Context) (*models.User, error)
	// Update(ctx *gin.Context, article *models.User) (*models.User, error)
	Store(ctx *gin.Context, a *models.User) (*models.User, error)
	// Delete(ctx *gin.Context) (bool, error)
}
