package config

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func Connect() (*gorm.DB, error) {
	db, error := gorm.Open("mysql", "root:password@/findme?charset=utf8&parseTime=True&loc=Local")
	return db, error
}
